package nl.apg.demo.controller;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import nl.apg.demo.data.entity.MetaData;
import nl.apg.demo.data.entity.UserInfo;
import java.util.List;
import nl.apg.demo.data.repository.MetaDataRepository;


@RestController
@RequestMapping("/api/metadata")
public class MetaDataController {
    @Autowired
    private MetaDataRepository metaRepo;

    @GetMapping
    public List<MetaData> getAll(){
        List<MetaData> metaList;
        metaList = metaRepo.findAll();
        return metaList;
    }

}
