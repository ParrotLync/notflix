package nl.apg.demo.controller;

import java.util.List;
import nl.apg.demo.data.entity.VideoComment;
import nl.apg.demo.data.repository.VideoItemRepository;
import nl.apg.demo.data.repository.VideoCommentRepository;
import nl.apg.demo.service.VideoCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/video")
public class VideoCommentRestController {
    @Autowired
    private VideoCommentService commentService;
    @Autowired
    private VideoItemRepository videoRepo;
    @Autowired
    private VideoCommentRepository commentRepo;

    @PostMapping("/{id}/comment")
    public VideoComment saveComment(@RequestBody VideoComment comment, @PathVariable String id) {
        if (videoRepo.findById(id) != null) {
            comment.setVideo(videoRepo.getOne(id));
            commentService.saveVideoComment(comment);
        }
        return comment;
    }

    @GetMapping("/{id}/{commentId}")
    public VideoComment getComment(@PathVariable String commentId) {
        return commentRepo.getOne(commentId);
    }

    @GetMapping("/{id}/comment")
    public List<VideoComment> getComments(@PathVariable String id) {
        return videoRepo.findVideoItemById(id).getCommentList();
    }
}
