package nl.apg.demo.controller;

import nl.apg.demo.service.VideoGenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import nl.apg.demo.service.VideoDescriptionService;
import nl.apg.demo.service.VideoItemService;
import nl.apg.demo.data.entity.VideoGenre;
import java.util.List;

@Controller
public class VideoTemplateController {
	@Autowired
	private VideoItemService videoService;
	@Autowired
	private VideoDescriptionService detailService;
	@Autowired
	private VideoGenreService genreService;

	@GetMapping("/home")
	public String main(Model model) {
		List<VideoGenre> genreList = genreService.getAllGenres();
		for (VideoGenre genre : genreList) {
			model.addAttribute(genre.getGenreName(), genre.getVideos());
		}
		model.addAttribute("genres", genreList);
		return "home";
	}

	@GetMapping("/template/video/{id}")
	public String getOne(Model model, @PathVariable String id) {
		model.addAttribute("video", detailService.getVideoDescription(id));
		return "videoTemplate";
	}
}
