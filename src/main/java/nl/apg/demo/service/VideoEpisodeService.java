package nl.apg.demo.service;
import nl.apg.demo.data.entity.*;
import nl.apg.demo.data.repository.VideoEpisodeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class VideoEpisodeService {
    @Autowired
    private VideoEpisodeRepository episodeRepo;

    public VideoSeason getSeason(String id){return episodeRepo.getOne(id).getSeason();}
    public VideoSerie getSerie(String id){return episodeRepo.getOne(id).getSeason().getSerie();}
}
