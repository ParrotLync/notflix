package nl.apg.demo.service;

import nl.apg.demo.data.entity.VideoEpisode;
import nl.apg.demo.data.entity.VideoSerie;
import nl.apg.demo.data.repository.VideoSeasonRepository;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VideoSeasonService {
    @Autowired
    private VideoSeasonRepository seasonRepo;

    public List<VideoEpisode> getEpisodes(String id){return seasonRepo.getOne(id).getEpisodes();}
    public VideoSerie getSerie(String id){return seasonRepo.getOne(id).getSerie();}
    public Integer getAmountOfEpisodes(String id){return seasonRepo.getOne(id).getEpisodes().size();}
}
