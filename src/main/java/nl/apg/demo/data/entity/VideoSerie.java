package nl.apg.demo.data.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class VideoSerie {

    // Single value attributes
    @Id
    private String id;
    private String title;
    private String description;
    private Integer year;
    private String network;
    private String imageSrc;
    private String imdbID;

    // Lists
    @OneToMany
    private List<VideoSeason> seasons;

    @ManyToMany
    private List<VideoGenre> videoGenres;

    public String getId() {return id;}
    public void setId(String id) {this.id = id;}

    public String getTitle() {return title;}
    public void setTitle(String title) {this.title = title;}

    public String getDescription() {return description;}
    public void setDescription(String description) {this.description = description;}

    public List<VideoGenre> getVideoGenres() {return videoGenres;}
    public void setVideoGenres(List<VideoGenre> videoGenres) {this.videoGenres = videoGenres;}

    public String getImageSrc() {return imageSrc;}
    public void setImageSrc(String imageSrc) {this.imageSrc = imageSrc;}

    public String getImdbID() {return imdbID;}
    public void setImdbID(String imdbID) {this.imdbID = imdbID;}

    public List<VideoSeason> getSeasons() {return seasons;}
    public void setSeasons(List<VideoSeason> seasons) {this.seasons = seasons;}

    public List<VideoGenre> getGenres() {return videoGenres;}
    public void setGenres(List<VideoGenre> genres) {this.videoGenres = genres;}

    public Integer getYear() {return year;}
    public void setYear(Integer year) {this.year = year;}

    public String getNetwork() {return network;}
    public void setNetwork(String network) {this.network = network;}
}
