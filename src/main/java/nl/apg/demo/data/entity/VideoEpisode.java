package nl.apg.demo.data.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class VideoEpisode {

    // Single value attributes
    @Id
    private String id;
    private String title;
    private String description;
    private String imagesrc;

    @ManyToOne
    private VideoItem video;
    @ManyToOne
    private VideoSeason season;

    // Getters & Setters
    public String getId() {return id;}
    public void setId(String id) {this.id = id;}

    public String getTitle() {return title;}
    public void setTitle(String title) {this.title = title;}

    public String getDescription() {return description;}
    public void setDescription(String description) {this.description = description;}

    public String getImagesrc() {return imagesrc;}
    public void setImagesrc(String imagesrc) {this.imagesrc = imagesrc;}

    public VideoItem getVideo() {return video;}
    public void setVideo(VideoItem video) {this.video = video;}

    public VideoSeason getSeason() {return season;}
    public void setSeason(VideoSeason season) {this.season = season;}
}
