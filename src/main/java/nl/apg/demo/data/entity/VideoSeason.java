package nl.apg.demo.data.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import java.util.List;

@Entity
public class VideoSeason {

    // Single value attributes
    @Id
    private String id;
    private Integer seasonNum;
    @ManyToOne
    private VideoSerie serie;

    // Lists
    @OneToMany
    private List<VideoEpisode> episodes;

    public String getId() {return id;}
    public void setId(String id) {this.id = id;}

    public Integer getSeasonNum() {return seasonNum;}
    public void setSeasonNum(Integer seasonNum) {this.seasonNum = seasonNum;}

    public VideoSerie getSerie() {return serie;}
    public void setSerie(VideoSerie serie) {this.serie = serie;}

    public List<VideoEpisode> getEpisodes() {return episodes;}
    public void setEpisodes(List<VideoEpisode> episodes) {this.episodes = episodes;}

}
