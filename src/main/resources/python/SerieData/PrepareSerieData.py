import SerieClasses
import requests
import json

baseURL = "https://api.thetvdb.com/"
serieSection = "series/#ID#"
episodeSection = "series/#ID#/episodes"
serieIDSection = "search/series?name=#NAME#"

seriesIMDBids = ["rick%20and%20morty", "the%20witcher%202019", "friends", "the%20mandolorean", "scrubs", "My%20Hero%20Academia", "heel%20hollands%20bakt", "chateau%20meiland", "vikings%202013", "peaky%20blinders"]
seriesTVDBids = []
def login():
    #Login
    loginURL = baseURL+"login"
    loginCred = json.dumps({'apikey':'dfbc4cb15f336c24e5a0b617542170f1',
                 'userkey':'5E247F2025CDA5.85933783',
                 'username':'raukost'})
    loginHeaders = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    x = requests.post(loginURL, data=loginCred, headers=loginHeaders)
    json_response = json.loads(x.text)
    token = json_response['token']
    return token

newToken = login()
headers = {'Accept': 'application/json', 'Authorization': 'Bearer '+newToken}

#Get tvdbID
for serieName in seriesIMDBids:
    serieID = serieIDSection.replace("#NAME#", serieName)
    tvdbURL = baseURL+serieID
    tvdbRequest = requests.get(url = tvdbURL, headers=headers)
    tvdbData = json.loads(tvdbRequest.content.decode('utf-8'))
    tvdb = tvdbData['data']
    
    print(tvdb)
    print()

# #Get data for serie
# serie = serieSection.replace("#ID#", "275274")
# url = baseURL+serie
# r = requests.get(url=url, headers=headers)
# data = json.loads(r.content.decode('utf-8'))
# serie_data = data['data']
# serie = SerieClasses.Serie(serie_data['id'], serie_data['seriesName'], serie_data['firstAired'], serie_data['overview'], serie_data['poster'])
#
# print('ID: ',serie.id)
# print('TVDBid: ',serie.tvDBid)
# print('TITLE: ',serie.title)
# print('YEAR: ',serie.year)
# print('DESC: ',serie.description)
# print('IMG: ',serie.serieIMG)
#
#
# episodes = episodeSection.replace("#ID#", "275274")
# second_url = "https://api.thetvdb.com/series/275274/episodes"
# epi = requests.get(url = second_url, headers = headers)
# data = json.loads(epi.content.decode('utf-8'))
# episodes_data = data['data']
#
# serie.addSeason(SerieClasses.Season(serie.id, 0))
# for episode in episodes_data:
#     added = False
#     for season in serie.seasons:
#         if season.seasonNum == episode['airedSeason']:
#             season.episodes.append(SerieClasses.Episode(episode['id'], season.seasonNum, episode['episodeName'], episode['overview'], "video"))
#             #print("Episode ",episode['episodeName'],"added to season", season.seasonNum)
#             #print("Season ",season.seasonNum, " has ", len(season.episodes), " episodes")
#             added = True
#
#     if not added:
#         serie.addSeason(SerieClasses.Season(serie.id, episode['airedSeason']))
#         #print("Added season!")
#         serie.seasons[-1].episodes.append(SerieClasses.Episode(episode['id'], serie.seasons[-1].seasonNum, episode['episodeName'], episode['overview'], "video"))
#         #print("Episode ",episode['episodeName'],"added to new season")
#         #print("Season ",season.seasonNum, " has ", len(season.episodes), " episodes")
#
#
# for season in serie.seasons:
#     print("Season: ",season.seasonNum)
#     print(len(season.episodes), " episodes")
#     print("EPISODES")
#     for episode in season.episodes:
#         print(episode.title)
#     print()
    

